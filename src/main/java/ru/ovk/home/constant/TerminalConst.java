package ru.ovk.home.constant;

public class TerminalConst {
    public static final String CMD_HELP      ="help";
    public static final String CMD_VERSION   ="version";
    public static final String CMD_ERROR     ="error";
    public static final String CMD_ABOUT     ="about";
    public static final String CMD_DEFAULT   ="default";
    public static final String CMD_EXIT      ="exit";

    public static final String PROJECT_CREATE ="project-create";
    public static final String PROJECT_CLEAR  ="project-clear";
    public static final String PROJECT_LIST   ="project-list";
    public static final String PROJECT_VIEW   ="project-view";
    public static final String PROJECT_VIEW_BY_INDEX    ="project-view-by-index";
    public static final String PROJECT_VIEW_BY_ID       ="project-view-by-id";
    public static final String PROJECT_VIEW_BY_NAME     ="project-view-by-name";
    public static final String PROJECT_REMOVE_BY_INDEX  ="project-remove-by-index";
    public static final String PROJECT_REMOVE_BY_ID     ="project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_NAME   ="project-remove-by-name";
    public static final String PROJECT_UPDATE_BY_INDEX  = "project-update-by-index";
    public static final String PROJECT_UPDATE_BY_ID     = "project-update-by-id";


    public static final String TASK_CREATE ="task-create";
    public static final String TASK_CLEAR  ="task-clear";
    public static final String TASK_LIST   ="task-list";
    public static final String TASK_VIEW   ="task-view";
    public static final String TASK_VIEW_BY_INDEX    ="task-view-by-index";
    public static final String TASK_VIEW_BY_ID       ="task-view-by-id";
    public static final String TASK_VIEW_BY_NAME     ="task-view-by-name";
    public static final String TASK_REMOVE_BY_INDEX  ="task-remove-by-index";
    public static final String TASK_REMOVE_BY_ID     ="task-remove-by-id";
    public static final String TASK_REMOVE_BY_NAME   ="task-remove-by-name";
    public static final String TASK_UPDATE_BY_INDEX  ="task-update-by-index";
    public static final String TASK_UPDATE_BY_ID     ="task-update-by-id";

    public static final String TELL_HELP     ="помощь";
    public static final String TELL_VERSION  ="версия";
    public static final String TELL_ERROR    ="параметр отсутствует";
    public static final String TELL_ABOUT    ="о программе";
    public static final String TELL_DEFAULT  ="по умолчанию";
    public static final String TELL_EXIT     ="выход";
};
