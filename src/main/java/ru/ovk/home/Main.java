package ru.ovk.home;

import ru.ovk.home.controller.*;
import ru.ovk.home.repository.*;
import ru.ovk.home.service.*;

import java.util.Scanner;
import static ru.ovk.home.constant.TerminalConst.*;

public class Main {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService       = new ProjectService(projectRepository);
    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskRepository taskRepository       = new TaskRepository();
    private final TaskService    taskService          = new TaskService(taskRepository);
    private final TaskController taskController       = new TaskController(taskService);

    private final SystemController systemController   = new SystemController();

    {
        projectService.create("Demo Project 1");
        projectService.create("Demo Project 2");
        taskService.create("TEST Task 1");
        taskService.create("TEST Task 2");
    }

    public static void main(String[] args) {
        final Scanner scaner = new Scanner(System.in);
        final Main main = new Main();
        main.run(args);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scaner.nextLine();
            main.run(command);
        }
    }

    private  void run(final String[] args) {
        String sCommand;
        if (args.length < 1) sCommand = "default";
        else sCommand = args[0];
        final int result = run(sCommand);
        //System.exit(result);
    }

    private int run(final String sCommand) {
        if(sCommand == null || sCommand.isEmpty() ) {return 0;}

        switch (sCommand) {
            case CMD_ABOUT:      return systemController.displayAbout();
            case CMD_VERSION:    return systemController.displayVersion();
            case CMD_HELP:       return systemController.displayHelp();
            case CMD_DEFAULT:    return systemController.displayDefault();
            case CMD_EXIT:       return systemController.displayExit();

            case PROJECT_CLEAR:            return projectController.clearProject();
            case PROJECT_CREATE:           return projectController.createProject();
            case PROJECT_LIST:             return projectController.listProject();
            case PROJECT_VIEW:             return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_INDEX:    return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:       return projectController.viewProjectById();
            case PROJECT_VIEW_BY_NAME:     return projectController.viewProjectByName();
            case PROJECT_REMOVE_BY_INDEX:  return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_ID:     return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_NAME:   return projectController.removeProjectByName();
            case PROJECT_UPDATE_BY_INDEX:  return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:     return projectController.updateProjectById();

            case TASK_CLEAR:               return taskController.clearTask();
            case TASK_CREATE:              return taskController.createTask();
            case TASK_LIST:                return taskController.listTask();
            case TASK_VIEW_BY_INDEX:       return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID:          return taskController.viewTaskById();
            case TASK_VIEW_BY_NAME:        return taskController.viewTaskByName();
            case TASK_REMOVE_BY_INDEX:     return taskController.removeTaskByIndex();
            case TASK_REMOVE_BY_ID:        return taskController.removeTaskById();
            case TASK_REMOVE_BY_NAME:      return taskController.removeTaskByName();
            case TASK_UPDATE_BY_INDEX:     return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID:        return taskController.updateTaskById();

            default:             return systemController.displayErr();
        }
    }




//======================================================================================================================



}