package ru.ovk.home.controller;
import ru.ovk.home.service.TaskService;
import ru.ovk.home.entity.Task;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    public  int createTask() {
        System.out.println("[Create Task]");
        System.out.println("inpunt task name");
        final String name = scaner.nextLine();
        taskService.create(name);

        System.out.println("[OK]");
        return 0;
    }

    public  int clearTask() {
        System.out.println("[Clear Task]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public  int listTask() {
        System.out.println("[List Task]");
        int index=1;
        for(final Task task: taskService.findAll()){
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public  void ViewTask(final Task task) {
        if(task==null) return;
        System.out.println("[View Tasks]");
        System.out.println( "ID: "   + task.getId());
        System.out.println( "NAME: " + task.getName());
        System.out.println( "DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public  int viewTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final int index  = scaner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        ViewTask(task);
        return 0;
    }
    public  int viewTaskById() {
        System.out.println("ENTER TASK ID");
        final Long id  =  scaner.nextLong();
        final Task task = taskService.findById(id);
        ViewTask(task);
        return 0;
    }

    public  int viewTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String name  =  scaner.nextLine();
        final Task task = taskService.findByName(name);
        ViewTask(task);
        return 0;
    }

    public  int removeTaskByIndex() {
        System.out.println("REMOVE TASK BY INDEX");
        System.out.println("ENTER TASK INDEX");
        final int index  = scaner.nextInt() - 1;
        final Task task  = taskService.removeByIndex(index);
        if( task==null) System.out.println("[FAIL]");
        else ViewTask(task);
        return 0;
    }

    public  int removeTaskById() {
        System.out.println("REMOVE TASK BY ID");
        System.out.println("ENTER TASK ID");
        final Long id  =  scaner.nextLong();
        final Task task = taskService.removeById(id);
        if( task==null) System.out.println("[FAIL]");
        else ViewTask(task);
        return 0;
    }

    public  int removeTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String name  =  scaner.nextLine();
        final Task task = taskService.removeByName(name);
        if( task==null) System.out.println("[FAIL]");
        else ViewTask(task);
        return 0;
    }

    public  int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("PLEASE ENTER TASK INDEX");
        final int index = Integer.parseInt(scaner.nextLine())-1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE ENTER TASK NAME");
        final String name = scaner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION");
        final String description = scaner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public  int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("PLEASE, ENTER TASK ID");
        final long id = Long.parseLong(scaner.nextLine());
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME");
        final String name = scaner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION");
        final String description = scaner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

}
