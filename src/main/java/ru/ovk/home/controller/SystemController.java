package ru.ovk.home.controller;

import static ru.ovk.home.constant.TerminalConst.*;

public class SystemController extends AbstractController {

    public int displayHelp() {
        System.out.println("\"project-create\"  create new project");
        System.out.println("\"project-list\"    list projects");
        System.out.println("\"project-clear\"   clear projects");
        System.out.println("\"project-view-by-index\"       view project by index");
        System.out.println("\"project-view-by-id\"          view project by id");
        System.out.println("\"project-view-by-name\"        view project by name");

        System.out.println("\"task-create\"         create new task");
        System.out.println("\"task-list\"           list tasks");
        System.out.println("\"task-clear\"          clear tasks");
        System.out.println("\"task-view-by-index\"  view task by index");
        System.out.println("\"task-view-by-id\"     view task by id");
        System.out.println("\"task-view-by-name\"   view task by name");
        System.out.println("\"task-remove-by-index\"  task remove by index");
        System.out.println("\"task-remove-by-id\"     task remove by id");
        System.out.println("\"task-remove-by-name\"   task remove by name");

        System.out.println("\"exit\"            exit from programm");

        return 0;
    }

    public  int displayAbout() {
        System.out.println(TELL_ABOUT);
        return 0;
    }

    public  int displayVersion() {
        System.out.println(TELL_VERSION);
        return 0;
    }

    public  int displayDefault() {
        System.out.println("Welcome");
        return 0;
    }

    public  int displayErr() {
        System.out.println(TELL_ERROR);
        return -1;
    }

    public  int displayExit() {
        System.out.println(TELL_EXIT);
        return 0;
    }

}
