package ru.ovk.home.repository;
import ru.ovk.home.entity.Project;


import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {
    private List<Project> projects = new ArrayList<>();

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public List<Project> findAll() {
        return projects;
    }

    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    public Project findByName(final String name) {
        for(final Project project: projects) {
            if(project.getName().equals(name)) return project;
        }
        return null;
    }
    public Project findById(final Long id) {
        for(final Project project: projects) {
            if(project.getId().equals(id)) return project;
        }
        return null;
    }
    public Project removeByIndex(final int id) {
        final Project project = findByIndex(id);
        if(project==null) return null;
        projects.remove(project);
        return project;
    }
    public Project removeById(final Long id) {
        final Project project = findById(id);
        if(project==null) return null;
        projects.remove(project);
        return project;
    }
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if(project==null) return null;
        projects.remove(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        Project project = findById(id);
        if(project==null) return  null;
        project.setName(name);
        project.setDescription(description);

        return project;
    }
}
